package com.example.gerar.appinterficie;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.gerar.appinterficie.SQLite.DAO.EstacionsConversor;
import com.example.gerar.appinterficie.SQLite.DAO.EstacionsSqliteHelper;
import com.example.gerar.appinterficie.SQLite.Constants;

import com.example.gerar.appinterficie.SQLite.Model.Estacio;
import com.example.gerar.appinterficie.SQLite.NovaEstacio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gerar on 18/02/2018.
 */

public class MainStation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private EstacionsSqliteHelper sqLiteHelper;
    private SimpleCursorAdapter adapter;
    private EstacionsConversor estacionsConversor;
    private ListView listView;
    private Cursor dades;
    private ActionMode mActionMode;
    private ActionMode.Callback implementActionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station);

        sqLiteHelper = new EstacionsSqliteHelper(getBaseContext(), Constants.DB_FILE_NAME, null, Constants.DB_VERSION);
        estacionsConversor = new EstacionsConversor(sqLiteHelper);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_station);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_station);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_station);
        navigationView.setNavigationItemSelectedListener(this);

//        List<Stations> stations = new ArrayList<Stations>();
//        stations.add(new Stations(R.drawable.space_station, "SX-457","Estació especial equipada amb la tecnologia més avançada pel que fa a energies especials i la ràpida càrrega de subministraments la fa una de les més importants de la ruta."));
//        stations.add(new Stations(R.drawable.space_station,"AR-7557","Una de les més grans de la galàxia perfecte per parar a descansar mentre la nau es recàrrega i desconnecta de la velocitat"));
//        stations.add(new Stations(R.drawable.space_station,"Galaxia3.0","La més antiga però això no vol dir la menys transitada, ha sabut mantenir-se i actualitzar-se sense problema"));

        //Crear adapter
        listView = ((ListView) findViewById(R.id.list));
        refreshData();
        listView.setSelected(true);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setAdapter(adapter);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean checked) {

                // Capture total checked items
                final int checkedCount = listView.getCheckedItemCount();
                // Set the title according to total checked items
                actionMode.setTitle(checkedCount + " seleccionats");

            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.contextual_listview, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.delete:
                        esborrarSeleccionats();
                        return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {

            }
        });
    }

    private void esborrarSeleccionats() {
        SparseBooleanArray seleccionats = listView.getCheckedItemPositions();
        for (int i = seleccionats.size() - 1; i >= 0; i--) {
            //titularsConversor.removeById(seleccionats.keyAt(i));
            if (seleccionats.valueAt(i)) {
                estacionsConversor.removeById((int) adapter.getItemId(i));
            }
        }
        refreshData();
    }

    private void refreshData() {
        String[] columnes = new String[]{"nom_estacio", "descripcio"};
        int[] views = new int[]{R.id.stationTitle, R.id.stationDescrip};

        dades = estacionsConversor.getAll();
        adapter = new SimpleCursorAdapter(getBaseContext(), R.layout.listview, dades,
                columnes, views, 0);
        listView.setAdapter(adapter);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_nigi);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//        Dialeg dialeg = new Dialeg();
//
//        dialeg.setMissatge(((Titular)adapter.getItem(position)).getTitol() + "\n" + ((Titular)adapter.getItem(position)).getSubtitol());
//        dialeg.setTitol("Titular seleccionat");
//        dialeg.show(getFragmentManager(),"" );
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.estacions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add: afegirEstacio(); break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void afegirEstacio() {
        Intent i = new Intent(getBaseContext(),NovaEstacio.class);
        startActivityForResult(i, 1);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.inici_activity) {
            Intent act2 = new Intent(this, MainActivity.class);
            startActivity(act2);

        } else if (id == R.id.planets_activity) {

            Intent act2 = new Intent(this, MainPlanets.class);
            startActivity(act2);

        } else if (id == R.id.station_activity) {

            Intent act2 = new Intent(this, MainStation.class);
            startActivity(act2);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_station);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Estacio e = (Estacio) data.getExtras().getSerializable("dades");
            estacionsConversor.save(e);
            refreshData();
        }
    }
}
