package com.example.gerar.appinterficie;


public class Planets {

    private int title, descripcio;
    private int thumbnail;


    public Planets( int title, int descripcio, int thumbnail ){
        this.title = title;
        this.descripcio= descripcio;
        this.thumbnail = thumbnail;
    }


    public int getTitol() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(int descripcio) {
        this.descripcio = descripcio;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
