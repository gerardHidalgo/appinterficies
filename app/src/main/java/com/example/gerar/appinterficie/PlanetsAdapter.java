package com.example.gerar.appinterficie;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PlanetsAdapter extends RecyclerView
        .Adapter<PlanetsAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "PlanetsAdapter";
    private ArrayList<Planets> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView titol;
        TextView descrip;
        ImageView thumb;

        public DataObjectHolder(View itemView) {
            super(itemView);
            titol = (TextView) itemView.findViewById(R.id.card_titol);
            descrip = (TextView) itemView.findViewById(R.id.card_description);
            thumb = (ImageView) itemView.findViewById(R.id.card_thumbnail);

            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public PlanetsAdapter(ArrayList<Planets> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.titol.setText(mDataset.get(position).getTitol());
        holder.descrip.setText(mDataset.get(position).getDescripcio());
        holder.thumb.setBackgroundResource(mDataset.get(position).getThumbnail());
    }

    public void addItem(Planets planet, int index) {
        mDataset.add(index, planet);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}