package com.example.gerar.appinterficie.SQLite;

/**
 * Created by gerar on 22/02/2018.
 */

public class Constants {
    public static final int DB_VERSION = 1;
    /**
     * nom del fitxer de BD que es crearà
     */
    public static final String DB_FILE_NAME = "Estacions.db";
}
