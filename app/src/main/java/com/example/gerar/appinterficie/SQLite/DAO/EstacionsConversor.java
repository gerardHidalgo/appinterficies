package com.example.gerar.appinterficie.SQLite.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.example.gerar.appinterficie.SQLite.Model.Estacio;

import java.util.ArrayList;


/**
 * Created by gerar on 22/02/2018.
 */

public class EstacionsConversor {

    private EstacionsSqliteHelper helper;

    public EstacionsConversor() {

    }

    public EstacionsConversor(EstacionsSqliteHelper helper) {
        this.helper = helper;
    }

    public long save(Estacio estacio){
        long index = -1;
        // s'agafa l'objecte base de dades en mode escriptura
        SQLiteDatabase db = helper.getWritableDatabase();
        // es crea un objecte de diccionari (clau,valor) per indicar els valors a afegir
        ContentValues dades = new ContentValues();

        dades.put("nom_estacio", estacio.getNom());
        dades.put("descripcio", estacio.getDescripcio());
        try {
            index = db.insertOrThrow("Estacions", null, dades);
            // exemple: volem veure en el log el que passa
            Log.i("Estacions", dades.toString() + " afegit amb codi " + index);
        }
        catch(Exception e) {
            // volem reflectir en ellog que hi ha hagut un error
            Log.e("Estacions", e.getMessage());
        }
        return index;
    }

    public Cursor getAll() {
        SQLiteDatabase db = helper.getReadableDatabase();

        return db.query(true, "Estacions",
                new String[]{"_id","nom_estacio","descripcio"},
                null, null, null, null, null, null);
    }

    /**
     * Obtenir tots els titulars en forma de llista
     * @return la llista que conté tots els titulars
     */
    public ArrayList<Estacio> getAllAsList() {
        ArrayList<Estacio> llista = new ArrayList<Estacio>();
        Cursor c = getAll();
        Estacio t = null;

        while(c.moveToNext()) {
            t = new Estacio(c.getInt(0), c.getString(1), c.getString(2));
            llista.add(t);
        }

        return llista;
    }

    /**
     * Obtenir un titular a partir del seu id (primary key)
     * @param id el codi de titular que es vol obtenir
     * @return el titular que coincideix amb l'id introduit o bé null si no el troba
     */
    public Estacio getById(int id) {
        SQLiteDatabase db = helper.getReadableDatabase();
        Estacio titular = null;
        Cursor c = db.query(true, "Estacions",
                new String[]{"_id","nom_estacio","descripcio"},
                "_id = ?", new String[]{id+""}, null, null, null, null);

        if(c.moveToNext()) {
            titular =  new Estacio(c.getInt(0), c.getString(1), c.getString(2));
        }
        return titular;
    }
    /**
     * Esborra el titular passat per paràmetre
     * @param id el codi titular a esborrar
     * @return la quantitat de titulars eliminats
     */
    public boolean removeById(int id) {
        // obtenir l'objecte BD en mode esriptura
        SQLiteDatabase db = helper.getWritableDatabase();

        return db.delete("Estacions", "_id=" + id,null ) > 0;
    }
    /**
     * Esborra tots els titulars de la taula
     * @return
     */
    public boolean removeAll() {
        // obtenir l'objecte BD en mode escriptura
        SQLiteDatabase db = helper.getWritableDatabase();

        return db.delete("Estacions", null, null ) > 0;
    }
}
