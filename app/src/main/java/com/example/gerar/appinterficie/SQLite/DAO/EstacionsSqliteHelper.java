package com.example.gerar.appinterficie.SQLite.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by gerar on 22/02/2018.
 */

public class EstacionsSqliteHelper extends SQLiteOpenHelper {

    private  final String SQL_CREATE_ESTACIONS = "CREATE TABLE Estacions("+
            "   _id INTEGER PRIMARY KEY, " +
            "   nom_estacio TEXT, " +
            "   descripcio TEXT)";


    public EstacionsSqliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ESTACIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versioAnterior, int versioNova) {
        db.execSQL("DROP TABLE IF EXISTS Estacions");
        db.execSQL(SQL_CREATE_ESTACIONS);
    }
}
