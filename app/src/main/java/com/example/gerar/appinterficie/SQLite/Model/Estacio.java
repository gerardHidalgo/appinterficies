package com.example.gerar.appinterficie.SQLite.Model;

import java.io.Serializable;

/**
 * Created by moha on 22/02/18.
 */

public class Estacio implements Serializable{
    private int codi;
    private String nom_estacio;
    private String descripcio;


    public Estacio(String est, String desc) {
        setNom(est);
        setDescripcio(desc);
    }

    public Estacio(int id, String est, String desc) {
        this(est, desc);
        setCodi(id);
    }

    // setters i getters

    public String getNom() {
        return nom_estacio;
    }

    public void setNom(String nom_estacio) {
        this.nom_estacio = nom_estacio;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

}
