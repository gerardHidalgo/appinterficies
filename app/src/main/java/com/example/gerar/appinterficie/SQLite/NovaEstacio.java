package com.example.gerar.appinterficie.SQLite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.gerar.appinterficie.R;
import com.example.gerar.appinterficie.SQLite.Model.Estacio;

/**
 * Created by moha on 22/02/18.
 */

public class NovaEstacio extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_estacio);
        Button b = (Button) findViewById(R.id.btnAcceptar);
        b.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nova_estacio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent();
        Bundle b = new Bundle();

        EditText nom_estacio = (EditText) findViewById(R.id.txtNomEstacio);
        EditText descripcio = (EditText) findViewById(R.id.txtDesc);

        Estacio e = new Estacio(nom_estacio.getText().toString(), descripcio.getText().toString());
        b.putSerializable("dades",e);
        i.putExtras(b);
        setResult(RESULT_OK, i);
        finish();
    }
}
