package com.example.gerar.appinterficie;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.gerar.appinterficie.SQLite.Model.Estacio;

import java.util.List;

/**
 * Created by gerar on 18/02/2018.
 */

public class StationAdapter extends ArrayAdapter<Estacio> {



    class Vista {
        public TextView titol;
        public TextView description;
    }

    public Estacio[] dades;

    StationAdapter(Activity context, Estacio[] dades) {
        super(context, R.layout.listview, dades);
        this.dades = dades;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View element = convertView;
        Vista vista;

        if (element == null) {
            LayoutInflater inflater = ((Activity) getContext())
                    .getLayoutInflater();
            element = inflater.inflate(R.layout.listview, null);

            vista = new Vista();
            vista.titol = (TextView) element.findViewById(R.id.stationTitle);
            vista.description = (TextView) element.findViewById(R.id.stationDescrip);

            element.setTag(vista);
        } else {
            vista = (Vista) element.getTag();
        }

        vista.titol.setText(dades[position].getNom());
        vista.description.setText(dades[position].getDescripcio());

        return element;
    }
}
