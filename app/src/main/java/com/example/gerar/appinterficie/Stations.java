package com.example.gerar.appinterficie;

/**
 * Created by gerar on 18/02/2018.
 */

public class Stations {
    private String title;
    private String descrip;
    private int img;

    public Stations(int img, String title, String descrip) {
        this.title = title;
        this.descrip = descrip;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
